$rgName="RG-AZ-204"
$location="westeurope"
$vmSize="Standard_B1s"
$winVmName="VM-WIN-WEU-C2-1"
$ubuVmName="VM-UBU-WEU-C2-1"
$winURN="MicrosoftWindowsServer:WindowsServer:2019-Datacenter:latest"
$ubuURN="Canonical:UbuntuServer:19.04:latest"
$creds = (Get-Credential -Message "Admin creds for VM:")
$text = "Hello world"
$userData = [System.Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($text))
$tag = @{"chapter"=2}

#Create RG
New-AzResourceGroup -Name $rgName -Location $location -Tag $tag -Force

#Create Win VM
New-AzVM -Name $winVmName -ResourceGroupName $rgName -Location $location -ImageName $winURN -Credential $creds -Size $vmSize -UserData $userData

#Create Ubuntu VM
New-AzVM -Name $ubuVmName -ResourceGroupName $rgName -Location $location -ImageName $ubuURN -Credential $creds -Size $vmSize -UserData $userData
